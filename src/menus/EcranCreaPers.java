package menus;

import client.Game;

// pour le moteur du jeu
import org.newdawn.slick.*;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.font.effects.ColorEffect;
import org.newdawn.slick.gui.TextField;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import java.awt.Font;
import java.sql.*;

// pour la fonctionnalit�s de la souris
import org.lwjgl.input.Mouse;

public class EcranCreaPers extends BasicGameState {
	
	public String mouse ="No imput yet";
	Image ecran;
	TextField id;
    UnicodeFont font;
    public String cree ="";
    public String nom;
    public int numId;
    boolean guerrier = false;
    boolean voleur = false;
    boolean mage = false;
    public int rectX,rectY;

	
	public EcranCreaPers(int state){
		
	}
	
	public void init(GameContainer gc, StateBasedGame sbg) throws SlickException{ // initialise ce dont on a besoin
		ecran = new Image("res/Ecran Creation.png");
		font = getNewFont("Arial" , 16);
	}
	
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g)throws SlickException{ // g�re l'affichage de l'�cran (les trucs affich�s a l'�cran quoi), couleur, police blabla
		ecran.draw(0,0,null);
		g.drawString(mouse, 50, 90);
		g.drawString(cree, 200, 480);
		id.render(gc , g);
        g.setFont(font);
        g.fillRect(rectX,rectY, 15, 15);

	}
	
	public void update(GameContainer gc, StateBasedGame sbg, int delta) throws SlickException{ // met � jour l'affichage � l'�cran
		int posX = Mouse.getX();
		int posY = Mouse.getY();
		font.loadGlyphs();
		mouse = "Position souris x : " + posX + " y : " + posY;
		
		// Cr�er
		if((posX>486 && posX<740) && (posY>67&& posY<163)){
			if(Mouse.isButtonDown(0) && Mouse.getEventButtonState() && !(id.getText().equals("")) && !( !mage && !guerrier && !voleur) ){
			   nom = id.getText();
			   if(guerrier){
				   try {
						Class.forName("oracle.jdbc.driver.OracleDriver");
					} catch (ClassNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					System.out.println("Pilote charg�");
					System.out.println();
					String url = "jdbc:oracle:thin:mvanzal/The619619@oracle.iut-orsay.fr:1521:etudom";
					try {
						Connection maConnexion = DriverManager.getConnection(url);
						CallableStatement lastColl = maConnexion.prepareCall(" {?= call lastCol}");
						lastColl.registerOutParameter(1,  java.sql.Types.INTEGER);
						lastColl.execute();
						numId = lastColl.getInt(1);
						numId = Game.idJoueur;
						System.out.println("last id: " + numId);
						
						CallableStatement creaGuer = maConnexion.prepareCall(" {call creaGuer(?,?)}");
						creaGuer.setString(1, nom);
						creaGuer.setInt(2,numId);
						creaGuer.execute();
						

						CallableStatement updateNumPerso = maConnexion.prepareCall(" {call updateNumPerso(?)}");
						updateNumPerso.setInt(1,numId);
						updateNumPerso.execute();
						
						lastColl.close();
						updateNumPerso.close();
						maConnexion.close();
						sbg.enterState(Game.ecranConn);
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			   }
			   else if (voleur){
				   try {
						Class.forName("oracle.jdbc.driver.OracleDriver");
					} catch (ClassNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					System.out.println("Pilote charg�");
					System.out.println();
					String url = "jdbc:oracle:thin:mvanzal/The619619@oracle.iut-orsay.fr:1521:etudom";
					try {
						Connection maConnexion = DriverManager.getConnection(url);
						CallableStatement lastColl = maConnexion.prepareCall(" {?= call lastCol}");
						lastColl.registerOutParameter(1,  java.sql.Types.INTEGER);
						lastColl.execute();
						numId = lastColl.getInt(1);
						numId = Game.idJoueur;
						
						CallableStatement creaVol= maConnexion.prepareCall(" {call creaVoleur(?,?)}");
						creaVol.setString(1, nom);
						creaVol.setInt(2,numId);
						creaVol.execute();

						CallableStatement updateNumPerso = maConnexion.prepareCall(" {call updateNumPerso(?)}");
						updateNumPerso.setInt(1,numId);
						updateNumPerso.execute();
						
						lastColl.close();
						creaVol.close();
						maConnexion.close();
						sbg.enterState(4);
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			   }
			   else if(mage){
				   try {
						Class.forName("oracle.jdbc.driver.OracleDriver");
					} catch (ClassNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					System.out.println("Pilote charg�");
					System.out.println();
					String url = "jdbc:oracle:thin:mvanzal/The619619@oracle.iut-orsay.fr:1521:etudom";
					try {
						Connection maConnexion = DriverManager.getConnection(url);
						CallableStatement lastColl = maConnexion.prepareCall(" {?= call lastCol}");
						lastColl.registerOutParameter(1,  java.sql.Types.INTEGER);
						lastColl.execute();
						numId = lastColl.getInt(1);
						numId = Game.idJoueur;
						
						CallableStatement creaMag= maConnexion.prepareCall(" {call creaMage(?,?)}");
						creaMag.setString(1, nom);
						creaMag.setInt(2,numId);
						creaMag.execute();

						CallableStatement updateNumPerso = maConnexion.prepareCall(" {call updateNumPerso(?)}");
						updateNumPerso.setInt(1,numId);
						updateNumPerso.execute();
						
						lastColl.close();
						creaMag.close();
						maConnexion.close();
						sbg.enterState(Game.ecranConn);
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			   }
			}
		}
		
		// Guerrier
		if((posX>0 && posX<307) && (posY>266 && posY<362)){
			if(Mouse.isButtonDown(0)){
				guerrier = true;
				voleur = false;
				mage = false;
				rectX = 195;
				rectY = 475;
			}
		}
		// Mage
		if((posX>273 && posX<526) && (posY>266 && posY<362)){
			if(Mouse.isButtonDown(0)){
				guerrier = false;
				voleur = false;
				mage = true;
				rectX = 466;
				rectY = 475;
			}
		}
		// Voleur
		if((posX>547 && posX<799) && (posY>266 && posY<362)){
			if(Mouse.isButtonDown(0)){
				guerrier = false;
				voleur = true;
				mage = false;
				rectX = 739;
				rectY = 475;
			}
		}
	
	}
	
	public void enter(GameContainer gc , StateBasedGame sbg)
            throws SlickException
    {
        id = new TextField(gc , font , 354 , 261, 200 , 35);
    }
	
	@SuppressWarnings("unchecked")
	public UnicodeFont getNewFont(String fontName , int fontSize)
    {
        font = new UnicodeFont(new Font(fontName , Font.PLAIN , fontSize));
        font.addGlyphs("@");
        font.getEffects().add(new ColorEffect(java.awt.Color.white));
        return (font);
    }
	
	public int getID(){ // donne l'identifiant de l'�cran menu qui est 0
		return Game.ecranCreaPers;
	}
}