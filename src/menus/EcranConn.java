package menus;

import client.Game;

// pour le moteur du jeu
import org.newdawn.slick.*;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.font.effects.ColorEffect;
import org.newdawn.slick.gui.TextField;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import java.awt.Font;
import java.sql.*;


// pour la fonctionnalit�s de la souris
import org.lwjgl.input.Mouse;

public class EcranConn extends BasicGameState {
	
	public String mouse ="No imput yet";
	Image ecran;
	TextField id;
    UnicodeFont font;
    public String reponse = "";
    public static String idEntre;
    public int verif=0;

	
	public EcranConn(int state){
		
	}
	
	public void init(GameContainer gc, StateBasedGame sbg) throws SlickException{ // initialise ce dont on a besoin
		ecran = new Image("res/Ecran Conn.png");
		font = getNewFont("Arial" , 16);
	}
	
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g)throws SlickException{ // g�re l'affichage de l'�cran (les trucs affich�s a l'�cran quoi), couleur, police blabla
		ecran.draw(0,0,null);
		g.drawString(mouse, 50, 90);
		id.render(gc , g);
        g.setFont(font);
        g.drawString(reponse, 250, 490);

	}
	
	public void update(GameContainer gc, StateBasedGame sbg, int delta) throws SlickException{ // met � jour l'affichage � l'�cran
		int posX = Mouse.getX();
		int posY = Mouse.getY();
		font.loadGlyphs();
		mouse = "Position souris x : " + posX + " y : " + posY;
		
		// Ok !
		if((posX>496 && posX<749) && (posY>106 && posY<198)){
			if(Mouse.isButtonDown(0) && Mouse.getEventButtonState() && !(id.getText().equals(""))){
				idEntre = id.getText();
				Game.idJoueur = Integer.parseInt(idEntre);
				try {
					Class.forName("oracle.jdbc.driver.OracleDriver");
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println("Pilote charg�");
				System.out.println();
				String url = "jdbc:oracle:thin:mvanzal/The619619@oracle.iut-orsay.fr:1521:etudom";
				try {
					Connection maConnexion = DriverManager.getConnection(url);
					CallableStatement verifieId = maConnexion.prepareCall(" {?= call verifID(?)}");
					verifieId.setInt(2, Integer.parseInt(idEntre));
					verifieId.registerOutParameter(1,  java.sql.Types.INTEGER);
					verifieId.execute();
					verif = verifieId.getInt(1);
					System.out.println(verif);
					verifieId.close();
					maConnexion.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if (verif == 1){
					sbg.enterState(Game.worldMap);
				}
				else{
					reponse = "Identifiant Incorrect !";
				}
			}
		}
	}
	
	public void enter(GameContainer gc , StateBasedGame sbg)
            throws SlickException
    {
        id = new TextField(gc , font , 339 , 335 , 200 , 35);
    }
	
	@SuppressWarnings("unchecked")
	public UnicodeFont getNewFont(String fontName , int fontSize)
    {
        font = new UnicodeFont(new Font(fontName , Font.PLAIN , fontSize));
        font.addGlyphs("@");
        font.getEffects().add(new ColorEffect(java.awt.Color.white));
        return (font);
    }
	
	public BasicGameState getSBG(){
		return this;
	}
	
	public int getID(){ // donne l'identifiant de l'�cran menu qui est 0
		return Game.ecranConn;
	}
}