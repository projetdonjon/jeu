package menus;

import client.Game;

// pour le moteur du jeu
import org.newdawn.slick.*;
import org.newdawn.slick.state.*;
import org.newdawn.slick.font.effects.ColorEffect;
import org.newdawn.slick.gui.ComponentListener;
import org.newdawn.slick.gui.TextField;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import java.awt.Font;

// pour la fonctionnalit�s de la souris
import org.lwjgl.input.Mouse;

public class EcranTitre extends BasicGameState {
	
	public String mouse ="No imput yet";
	Image ecran;
	
	public EcranTitre(int state){
		
	}
	
	public void init(GameContainer gc, StateBasedGame sbg) throws SlickException{ // initialise ce dont on a besoin
		ecran = new Image("res/Ecran Titre.png");
	}
	
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g)throws SlickException{ // g�re l'affichage de l'�cran (les trucs affich�s a l'�cran quoi), couleur, police blabla
		ecran.draw(0,0,null);
		g.drawString(mouse, 50, 90);
	}
	
	public void update(GameContainer gc, StateBasedGame sbg, int delta) throws SlickException{ // met � jour l'affichage � l'�cran
		int posX = Mouse.getX();
		int posY = Mouse.getY();
		mouse = "Position souris x : " + posX + " y : " + posY;
		
		// S'enregistrer
		if((posX>110 && posX<363) && (posY>242 && posY<333)){
			if(Mouse.isButtonDown(0) && Mouse.getEventButtonState()){
				sbg.enterState(Game.ecranEnreg);
			}
		}
		
		// Se connecter
		if((posX>440 && posX<690) && (posY>242 && posY<333)){
			if(Mouse.isButtonDown(0) && Mouse.getEventButtonState()){
				sbg.enterState(Game.ecranConn);
			}
		}
		
		// Aller direct sur la map
		if (gc.getInput().isKeyDown(Input.KEY_P)){
			sbg.enterState(Game.worldMap);
		}
	
	}
	
	
	public int getID(){ // donne l'identifiant de l'�cran menu qui est 0
		return Game.ecranTitre;
	}
}