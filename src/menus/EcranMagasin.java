package menus;

import client.Game;

// pour le moteur du jeu
import org.newdawn.slick.*;
import org.newdawn.slick.state.*;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.font.effects.ColorEffect;
import org.newdawn.slick.gui.ComponentListener;
import org.newdawn.slick.gui.TextField;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import java.awt.Font;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import menus.EcranConn;

// pour la fonctionnalit�s de la souris
import org.lwjgl.input.Mouse;

public class EcranMagasin extends BasicGameState {
	
	public String mouse ="No imput yet";
	Image ecran;
	
	public EcranMagasin(int state){
		
	}
	
	public void init(GameContainer gc, StateBasedGame sbg) throws SlickException{ // initialise ce dont on a besoin
		ecran = new Image("res/Ecran Magasin.png");
	}
	
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g)throws SlickException{ // g�re l'affichage de l'�cran (les trucs affich�s a l'�cran quoi), couleur, police blabla
		ecran.draw(0,0,null);
		g.drawString(mouse, 50, 90);
	}
	
	public void update(GameContainer gc, StateBasedGame sbg, int delta) throws SlickException{ // met � jour l'affichage � l'�cran
		int posX = Mouse.getX();
		int posY = Mouse.getY();
		mouse = "Position souris x : " + posX + " y : " + posY;
		
		// Retour
		if((posX>570 && posX<775) && (posY>19 && posY<49)){
			if(Mouse.isButtonDown(0)){
				sbg.enterState(Game.worldMap);
			}
		}
	}
	

	
	
	public int getID(){ // donne l'identifiant de l'�cran menu qui est 0
		return Game.ecranMagasin;
	}
}