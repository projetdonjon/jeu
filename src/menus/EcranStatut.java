package menus;

import client.Game;


// pour le moteur du jeu
import org.newdawn.slick.*;
import org.newdawn.slick.state.*;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.font.effects.ColorEffect;
import org.newdawn.slick.gui.ComponentListener;
import org.newdawn.slick.gui.TextField;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import java.awt.Font;
import java.io.File;
import java.io.PrintStream;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import menus.EcranConn;


// pour la fonctionnalit�s de la souris
import org.lwjgl.input.Mouse;

public class EcranStatut extends BasicGameState {
	
	public String mouse ="No imput yet";
	Image ecran;
	String statut = "";
	String comp = "";
	String nom,classe;
	int lvl,atk,def,pv,xp,agi,rap,arg;
	
	
	public EcranStatut(int state){
		
	}
	

	public static void redirigerErreur()
	{		
		try
		{
		    System.setErr(new PrintStream(new File("output-error.txt")));
		}
		catch (Exception e)
		{
		     e.printStackTrace();
		}
	}
	
	public void init(GameContainer gc, StateBasedGame sbg) throws SlickException{ // initialise ce dont on a besoin
		ecran = new Image("res/Ecran Statut.png");
	}
	
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g)throws SlickException{ // g�re l'affichage de l'�cran (les trucs affich�s a l'�cran quoi), couleur, police blabla
		ecran.draw(0,0,null);
		g.drawString(mouse, 50, 90);
		g.drawString(statut, 50, 120);
		g.drawString(comp, 200, 120);
	}
	
	public void update(GameContainer gc, StateBasedGame sbg, int delta) throws SlickException{ // met � jour l'affichage � l'�cran
		redirigerErreur();
		int posX = Mouse.getX();
		int posY = Mouse.getY();
		mouse = "Position souris x : " + posX + " y : " + posY;
	
		// Retour
				if((posX>570 && posX<775) && (posY>19 && posY<49)){
					if(Mouse.isButtonDown(0)){
						sbg.enterState(Game.worldMap);
					}
				}
		// Affichage Statut
				try {
					Class.forName("oracle.jdbc.driver.OracleDriver");
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println("Pilote charg�");
				System.out.println();
				String url = "jdbc:oracle:thin:mvanzal/The619619@oracle.iut-orsay.fr:1521:etudom";
				try {
					Connection maConnexion = DriverManager.getConnection(url);
					CallableStatement getNom = maConnexion.prepareCall(" {?= call getNomPers(?)}");
					getNom.setInt(2, (Game.idJoueur));
					getNom.registerOutParameter(1,  java.sql.Types.VARCHAR);
					getNom.execute();
					nom = getNom.getString(1);
					getNom.close();
					
					CallableStatement getClas = maConnexion.prepareCall(" {?= call getClasPers(?)}");
					getClas.setInt(2, Game.idJoueur);
					getClas.registerOutParameter(1,  java.sql.Types.VARCHAR);
					getClas.execute();
					classe = getClas.getString(1);
					getClas.close();
					
					CallableStatement getlvl = maConnexion.prepareCall(" {?= call getlvlPers(?)}");
					getlvl.setInt(2, Game.idJoueur);
					getlvl.registerOutParameter(1,  java.sql.Types.INTEGER);
					getlvl.execute();
					lvl = getlvl.getInt(1);
					getlvl.close();
					
					CallableStatement getAtk = maConnexion.prepareCall(" {?= call getAtkPers(?)}");
					getAtk.setInt(2, Game.idJoueur);
					getAtk.registerOutParameter(1,  java.sql.Types.INTEGER);
					getAtk.execute();
					atk = getAtk.getInt(1);
					getAtk.close();
					
					CallableStatement getDef = maConnexion.prepareCall(" {?= call getDefPers(?)}");
					getDef.setInt(2, Game.idJoueur);
					getDef.registerOutParameter(1,  java.sql.Types.INTEGER);
					getDef.execute();
					def = getDef.getInt(1);
					getDef.close();
					
					CallableStatement getPv = maConnexion.prepareCall(" {?= call getPvPers(?)}");
					getPv.setInt(2, Game.idJoueur);
					getPv.registerOutParameter(1,  java.sql.Types.INTEGER);
					getPv.execute();
					pv = getPv.getInt(1);
					getPv.close();
					
					CallableStatement getXp = maConnexion.prepareCall(" {?= call getXpPers(?)}");
					getXp.setInt(2, Game.idJoueur);
					getXp.registerOutParameter(1,  java.sql.Types.INTEGER);
					getXp.execute();
					xp = getXp.getInt(1);
					getXp.close();
					
					CallableStatement getAgi = maConnexion.prepareCall(" {?= call getAgiPers(?)}");
					getAgi.setInt(2, Game.idJoueur);
					getAgi.registerOutParameter(1,  java.sql.Types.INTEGER);
					getAgi.execute();
					agi = getAgi.getInt(1);
					getAgi.close();
					
					CallableStatement getRap = maConnexion.prepareCall(" {?= call getRapPers(?)}");
					getRap.setInt(2, Game.idJoueur);
					getRap.registerOutParameter(1,  java.sql.Types.INTEGER);
					getRap.execute();
					rap = getRap.getInt(1);
					getRap.close();
					
					CallableStatement getArg = maConnexion.prepareCall(" {?= call getArgPers(?)}");
					getArg.setInt(2, Game.idJoueur);
					getArg.registerOutParameter(1,  java.sql.Types.INTEGER);
					getArg.execute();
					arg = getArg.getInt(1);
					getArg.close();
					maConnexion.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				statut = "Nom personnage : " +nom+ "\nClasse : " +classe+"\nNiveau : " +lvl+"\nXP : "+xp+ "\nPoints de Vie : "+pv+"\nAttaque : "+atk+"\nD�fense : "+def+"\nAgilit� : "+agi+"\nRapidit� : "+rap+"\nArgent : "+arg;
	}
	
	
	public int getID(){ // donne l'identifiant de l'�cran menu qui est 0
		return Game.ecranStatut;
	}
}