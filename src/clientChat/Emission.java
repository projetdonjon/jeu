package clientChat;

import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;


public class Emission implements Runnable {

	private PrintWriter out;
	private String login = null, message = null;
	private Scanner sc = null;
	private Socket socket;
	private Joueur player;
	
	public Emission(PrintWriter out, Socket socket, Joueur player) {
		this.out = out;
		this.socket = socket;
		this.player = player;
	}

	
	public void run() {
		
		  sc = new Scanner(System.in);
		  //message =player.toString();

		  message="";
		  while(!message.equals("exit")){
			  	login = socket.getLocalPort()+"";
			    System.out.println(login + " dit :");
				message = sc.nextLine();
			    
				out.println(message);
			    out.flush();
			  }
		  Thread.currentThread().interrupt();
	}
}
