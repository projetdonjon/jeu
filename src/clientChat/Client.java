package clientChat;

import java.io.*;
import java.net.*;
import java.util.Scanner;

public class Client implements Runnable {
	

	public static Socket socket = null;
	public static Thread t1;
	public static String login;
	private static PrintWriter out = null;
	private static BufferedReader in = null;
	private static Scanner sc = null;
	private static Boolean connect = false;
	private static Joueur player;

	
	public Client()
	{
		
	}
	public void run(){

		try {

			// Connexion TCP
			//------------------------------------------------------------------

			System.out.println("Demande de connexion");
			socket = new Socket("192.168.137.187", 2009);
			// Si le message s'affiche c'est que je suis connect�
			System.out.println("Connexion �tablie avec le serveur");
			System.out.println("l'adress du client distant est la suivante: "+ socket.getInetAddress());
			System.out.println("l'adress du client distant est la suivante: "+ socket.getLocalSocketAddress());
			

			//Choix du login
			//------------------------------------------------------------------
			out = new PrintWriter(socket.getOutputStream());
			in = new BufferedReader(new InputStreamReader(
					socket.getInputStream()));
			sc = new Scanner(System.in);
			
			while (!connect) {

				System.out.println(in.readLine());
				login = sc.nextLine();
				out.println(login);
				out.flush();
				connect = true;
			}
			//Creattion du joueur
			//------------------------------------------------------------------
			Joueur player1 = new Joueur(login,login,101,101);
			player = player1;

			//D�but du thread de communication
			//------------------------------------------------------------------

			t1 = new Thread(new Chat_ClientServeur(socket, player));
			t1.start();
			
			//Boucle infinie permettant d'arreter le thread une fois le chat fini
			while (true){
				if (!t1.isAlive()) {
					System.out.println("le t1 est mort");
					break;
				}
				
			}
			
			//terminer le client
			Thread.currentThread().interrupt();
			socket.close();

			
			//Exceptions
			//------------------------------------------------------------------

		} catch (UnknownHostException e) {
			System.err.println("Impossible de se connecter � l'adresse "
					+ socket.getLocalAddress());
		} catch (IOException e) {
			System.err.println("Aucun serveur � l'�coute du port "
					+ socket.getLocalPort());
		}

	}

}
