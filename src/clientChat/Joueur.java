package clientChat;

public class Joueur {
	private String nom, prenom;
	private int numID;
	private int id;
	
	private double coordX,coordY;


	public Joueur(String nom, String prenom, int numID, int id, double coordX,
			double coordY) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.numID = numID;
		this.id = id;
	}
	public Joueur(String nom, String prenom, int numID, int id) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.numID = numID;
		this.id = id;
	}

	public double getCoordX() {
		return coordX;
	}

	public void setCoordX(double coordX) {
		this.coordX = coordX;
	}

	public double getCoordY() {
		return coordY;
	}

	public void setCoordY(double coordY) {
		this.coordY = coordY;
	}

	@Override
	public String toString() {
		return "numID=" + numID+ ";id=" + id + ";cX=" + coordX + ";cY=" + coordY;
	}
	

}
