package clientChat;

import java.io.*;
import java.net.*;
import java.util.Scanner;

public class Chat_ClientServeur implements Runnable {

	private Socket socket;
	private PrintWriter out = null;
	private BufferedReader in = null;
	private Thread t3, t4;
	private Joueur player;

	public Chat_ClientServeur(Socket s, Joueur player) {
		socket = s;
		this.player = player;
	}

	public void run() {
		try {
			out = new PrintWriter(socket.getOutputStream());
			in = new BufferedReader(new InputStreamReader(
					socket.getInputStream()));
			

				t4 = new Thread(new Emission(out, socket, player));
				t4.start();
				t3 = new Thread(new Reception(in, socket));
				t3.start();
				
			while (true){
				if (!t4.isAlive()) {
					System.out.println("le t4 est mort");
					t3.interrupt();
					
					break;
				}
				
			}
			Thread.currentThread().interrupt();
			

		} catch (IOException e) {
			System.err.println("Le serveur distant s'est déconnecté !");
		}
	}

}
