package client;

import org.newdawn.slick.Animation;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

public class Personnage {
	Animation perso, movingUp, movingDown, movingLeft, movingRight;
	private int[] duration = {200,200};
	private float x = 11;
	private float y = 36;
	private int direction = 0;
	private boolean moving = false;
	private Map map;

	public Personnage(Map map) {
	    this.map = map;
	 }
	
	public void initRanger() throws SlickException {
		Image[] walkUp = {new Image("res/rangerh1.png"), new Image("res/rangerh1.png")};
		Image[] walkDown = {new Image("res/rangerb1.png"), new Image("res/rangerb1.png")};
		Image[] walkLeft = {new Image("res/rangerg1.png"), new Image("res/rangerg1.png")};
		Image[] walkRight = {new Image("res/rangerd1.png"), new Image("res/rangerd1.png")};
		movingUp = new Animation(walkUp,duration,false);
		movingDown = new Animation(walkDown,duration,false);
		movingLeft = new Animation(walkLeft,duration,false);
		movingRight = new Animation(walkRight,duration,false);
		perso = movingDown;
	 }
	
	public void render(Graphics g) throws SlickException {
		perso.draw(x*16, y*16);
	}
	
	public void update(int delta) {
	//	map.getTileId(18, 20, map.getLayerColl());

	}
		}
