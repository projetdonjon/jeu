package client;

import client.Game;

import org.newdawn.slick.Animation;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.tiled.TiledMap;


public class WorldMap extends BasicGameState {
	
	private TiledMap map;
	private Animation ranger, movingUp, movingDown, movingLeft, movingRight;
	int[] duration = {200,200};
	private float x,y;
	private final int xInit = 11;
	private final int yInit = 36;
	String combat = "";
	String message1 = "Pour ouvrir l'inventaire appuyez sur 'I'";
	String message2 = "Pour voir le statut de votre personnage appuyez sur 'C'";

	public WorldMap(int state){
		
	}
	
	public void init(GameContainer gc, StateBasedGame sbg) throws SlickException{ 
		map = new TiledMap("res/Map.tmx");
		x = xInit;
		y = yInit;
	
		
		Image[] walkUp = {new Image("res/rangerh1.png"), new Image("res/rangerh1.png")};
		Image[] walkDown = {new Image("res/rangerb1.png"), new Image("res/rangerb1.png")};
		Image[] walkLeft = {new Image("res/rangerg1.png"), new Image("res/rangerg1.png")};
		Image[] walkRight = {new Image("res/rangerd1.png"), new Image("res/rangerd1.png")};
		
		movingUp = new Animation(walkUp,duration,false);
		movingDown = new Animation(walkDown,duration,false);
		movingLeft = new Animation(walkLeft,duration,false);
		movingRight = new Animation(walkRight,duration,false);
		
		ranger = movingDown;
	}
	
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g)throws SlickException{ // g�re l'affichage de l'�cran principal, couleur, police blabla
		map.render(0, 0);
		ranger.draw(x*16, y*16);
		g.drawString("Perso X : " +x+"\nPerso Y : " +y, 300, 50);
		g.drawString(combat,400,500);
		g.drawString(message1, 170, 745);
		g.drawString(message2, 170, 765);
	}
	
	public void update(GameContainer gc, StateBasedGame sbg, int delta) throws SlickException{ // met � jour l'affichage � l'�cran
		// objets sur la map
		int collisionLayer = map.getLayerIndex("collision");
		
		// mouvements du personnage
		map.getTileId(18, 20, collisionLayer);
		if (gc.getInput().isKeyDown(Input.KEY_RIGHT)){
			ranger = movingRight;
			if (map.getTileId((int)x+1,(int) y, collisionLayer)==0){
				x = (float) (x +0.01);
			}
		}
		if (gc.getInput().isKeyDown(Input.KEY_LEFT)){
			ranger = movingLeft;
			if (map.getTileId((int)x-(int)0.01, (int)y, collisionLayer)==0){
				x = (float) (x -0.01);
			}
		}
		if (gc.getInput().isKeyDown(Input.KEY_UP)){
			ranger = movingUp;
			if (map.getTileId((int)x, (int)y-(int)0.01, collisionLayer)==0){
				y = (float) (y -0.01);
			}
		}
		if (gc.getInput().isKeyDown(Input.KEY_DOWN)){
			ranger = movingDown;
			if (map.getTileId((int)x, (int)y+1, collisionLayer)==0){
				y = (float) (y+0.01);
			}
		}
		
		// Interdire les mouvements en diagonale
		
		if ((gc.getInput().isKeyDown(Input.KEY_RIGHT)) && (gc.getInput().isKeyDown(Input.KEY_UP))){
			x = (float) (x -0.01);
			y = (float) (y +0.01);
		}
		if ((gc.getInput().isKeyDown(Input.KEY_RIGHT)) && (gc.getInput().isKeyDown(Input.KEY_DOWN))){
			x = (float) (x -0.01);
			y = (float) (y -0.01);
		}
		if ((gc.getInput().isKeyDown(Input.KEY_LEFT)) && (gc.getInput().isKeyDown(Input.KEY_DOWN))){
			x = (float) (x +0.01);
			y = (float) (y - 0.01);
		}
		if ((gc.getInput().isKeyDown(Input.KEY_LEFT)) && (gc.getInput().isKeyDown(Input.KEY_UP))){
			x = (float) (x +0.01);
			y = (float) (y +0.01);
		}
		
		// Mettre un �v�nement sur les buissons
		int sBackLayer = map.getLayerIndex("subBackground");
		map.getTileId(12,24, sBackLayer);
		if (map.getTileId((int)x+(int)0.75, (int)y+(int)1, sBackLayer)!=0){
			combat = "Combat !";
		}
		else{
			combat = "";
		}
		
		// Mettre un �v�nement sur l'entr�e du village
				int magasinLayer = map.getLayerIndex("magasin");
				map.getTileId(25,28, magasinLayer);
				if (map.getTileId((int)x+(int)0.75, (int)y+(int)1, magasinLayer)!=0){
					x = 25;
					y = 30;
					sbg.enterState(Game.ecranMagasin);
				}
		
		// Consulter l'inventaire
		if (gc.getInput().isKeyDown(Input.KEY_I)){
			sbg.enterState(Game.ecranInven);
		}
		
		// Consulter le statut du personnage
				if (gc.getInput().isKeyDown(Input.KEY_C)){
					sbg.enterState(Game.ecranStatut);
				}
		
	}
	
	public int getID(){ 
		return Game.worldMap;
	}

}
