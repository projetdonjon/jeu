package client;

import java.io.File;
import java.io.PrintStream;

import org.newdawn.slick.*;
import org.newdawn.slick.state.*;

import clientChat.Client;
import menus.EcranConn;
import menus.EcranCreaPers;
import menus.EcranEnreg;
import menus.EcranInven;
import menus.EcranScen;
import menus.EcranStatut;
import menus.EcranTitre;
import menus.EcranMagasin;






public class Game extends StateBasedGame{
	
	public static final String gamename = "Jeu MUD";
	public static final int ecranTitre = 0; // identifiants �crans (menu, en jeu)
	public static final int ecranEnreg = 1;
	public static final int ecranScen = 2;
	public static final int ecranCreaPers = 3;
	public static final int ecranConn = 4;
	public static final int worldMap = 5;
	public static final int ecranInven = 6;
	public static final int ecranStatut = 7;
	public static final int ecranMagasin = 8;
	
	public static int  idJoueur;
	
	public Game(String gamename){ // permet de nommer le nom de la fenetre du jeu
		super(gamename); // appelle la classe au dessus
		this.addState(new EcranTitre(ecranTitre));
		this.addState(new EcranEnreg(ecranEnreg));
		this.addState(new EcranScen(ecranScen));
		this.addState(new EcranCreaPers(ecranCreaPers));
		this.addState(new EcranConn(ecranConn));
		this.addState(new WorldMap(worldMap));
		this.addState(new EcranInven(ecranInven));
		this.addState(new EcranStatut(ecranStatut));
		this.addState(new EcranMagasin(ecranMagasin));
		
	}
	
	public void initStatesList(GameContainer gc) throws SlickException{ // cr�e le moteur du jeu, g�re la navigation entre fen�tres etc...
		this.enterState(ecranTitre); // initialise l'�cran de d�part !
		this.getState(ecranTitre).init(gc, this);
		this.getState(ecranEnreg).init(gc, this);
		this.getState(ecranScen).init(gc, this);
		this.getState(ecranCreaPers).init(gc, this);
		this.getState(ecranConn).init(gc, this);
		this.getState(worldMap).init(gc, this);
		this.getState(ecranInven).init(gc, this);
		this.getState(ecranStatut).init(gc, this);
		this.getState(ecranMagasin).init(gc, this);
	}

	public static void redirigerSortie()
	{		
		try
		{
		    System.setOut(new PrintStream(new File("output-file.txt")));
		    System.setErr(new PrintStream(new File("output-errors.txt")));
		}
		catch (Exception e)
		{
		     e.printStackTrace();
		}
	}

	public static Thread clientChat;
	
	public static void main(String[] args) {
		
		clientChat = new Thread(new Client());
		clientChat.start();

		redirigerSortie();
		
		AppGameContainer appgc; // fenetre du jeu
		try{
			appgc = new AppGameContainer(new Game(gamename)); // cr�e une fenetre qui contient le nouveau jeu
			appgc.setDisplayMode(800, 800, false /* plein �cran */); // dimensions de la fenetre
			appgc.start(); // lancer la fenetre
		}catch(SlickException e){
			e.printStackTrace();
		}
		
	}

}
