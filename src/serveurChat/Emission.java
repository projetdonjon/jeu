package serveurChat;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Vector;
import java.util.concurrent.Semaphore;

public class Emission implements Runnable {

	private PrintWriter out;
	private String message = null;
	private Semaphore mutex = null;
	private Socket socket;

	private Vector<Socket> clients = null;
	private Vector<String> messages = null;

	public Emission( Vector<Socket> clients,
			Vector<String> messages) {
		this.clients = clients;
		this.messages = messages;
		this.mutex = new Semaphore(1, true);
	}

	public void run() {
		try {
			while (true) {

				if (!messages.isEmpty()) {
					mutex.acquire();
					System.out
							.println("--------------------------------------------------------------");
					for (String s : messages) {

						System.out.println("message " + s);
					}
					System.out
							.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
					for (Socket s : clients) {
						System.out.println(s.getPort());
					}
					System.out
							.println("--------------------------------------------------------------");
					
					message = messages.elementAt(0);
					
					for (Socket s : clients) {
						//if (!(s.getPort() == socket.getPort())) {
							out = new PrintWriter(s.getOutputStream());

							out.println(message);
							out.flush();

							System.out.println("message envoy� � "
									+ s.getPort() + ":");
							System.out.println(message);
						//}
					}
					
					if (!messages.isEmpty()) {
						messages.removeElementAt(0);
					}
					mutex.release();
				}

			}

		} catch (IOException | InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			clients.remove(socket);
		}
	}
}
