package serveurChat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Vector;

public class Chat_ServeurClient implements Runnable {

	private Socket socket = null;
	private BufferedReader in = null;
	private PrintWriter out = null;

	private Vector<Socket> clients;
	private Vector<String> messages;
	private String login;

	private Thread t3, t4;

	public Chat_ServeurClient(Socket s, Vector<Socket> clients,
			Vector<String> messages) {
		socket = s;
		this.clients = clients;
		this.messages = messages;
	}

	public void run() {


		try {
			in = new BufferedReader(new InputStreamReader(
					socket.getInputStream()));
			out = new PrintWriter(socket.getOutputStream());
			
			

			out.println("Entrez votre login :");
			out.flush();
			login = in.readLine();


			t3 = new Thread(new Reception(in, login, clients, messages));
			t3.start();
			//t4 = new Thread(new Emission(login, clients, messages, socket));
			//t4.start();

			while (true) {
				if (!t3.isAlive()) {
					//t4.interrupt();
					Thread.currentThread().interrupt();
					break;
				}
			}

		} catch (IOException e) {
			System.err.println(login + "s'est déconnecté ");

		}
	}
}