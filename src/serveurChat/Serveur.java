package serveurChat;

import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.Vector;

import clientChat.Client;

public class Serveur {
 public static ServerSocket ss = null;
 public static Thread t,t4;
 private static Socket socket = null;
 private static Vector<Socket> clients;
 private static Vector<String> messages;

 
	public static void main(String[] args) {
		
		try {
			//cr�ation de la socket
			ss = new ServerSocket(2009);
			System.out.println("Le serveur est � l'�coute du port "+ss.getLocalPort());
			
			//cr�ation du stockage des clients et des messages
			clients = new Vector<Socket>();
			messages = new Vector<String>();

			//lancement du thread d'emission
			t4 = new Thread(new Emission(clients, messages));
			t4.start();
			
			//accepter une nouvelle connexion
			while(true){
				socket = ss.accept();
				clients.add(socket);
				
				System.out.println("TENTATIVE DE CONNEXION");
				
				t = new Thread(new Chat_ServeurClient(socket, clients, messages));
				t.start();
				
			}
			
		} catch (IOException e) {
			System.err.println("Le port "+ss.getLocalPort()+" est d�j� utilis� !");
		}
	
	}

	
	}
